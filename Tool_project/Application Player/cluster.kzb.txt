﻿Files:
PropertyTypeLibrary/BatteryCharge
PropertyTypeLibrary/BeamHeadlights
PropertyTypeLibrary/ClusterSongIndex
PropertyTypeLibrary/ClusterSongName
PropertyTypeLibrary/CubemapColor
PropertyTypeLibrary/DriveMode
PropertyTypeLibrary/IconLovBeamHeadlights
PropertyTypeLibrary/IconsAbs
PropertyTypeLibrary/IconsBeamHeadlights
PropertyTypeLibrary/IconsBelt
PropertyTypeLibrary/IconsBrake
PropertyTypeLibrary/IconsCheck
PropertyTypeLibrary/IconsFogLights
PropertyTypeLibrary/IconsHighBeam
PropertyTypeLibrary/IconsOil
PropertyTypeLibrary/IconsParkingLights
PropertyTypeLibrary/LeftLocked
PropertyTypeLibrary/Lockers.LeftLocked
PropertyTypeLibrary/Lockers.LeftLocked_1
PropertyTypeLibrary/Lockers.RightLocked
PropertyTypeLibrary/Lockers.RightLocked_1
PropertyTypeLibrary/MusicPlayer.ItemsSource
PropertyTypeLibrary/MusicPlayer.MusicPlayer.Cover
PropertyTypeLibrary/MusicPlayer.MusicPlayer.ItemsSource
PropertyTypeLibrary/MusicPlayer.MusicPlayer.WorldFadeRange
PropertyTypeLibrary/MusicPlayer.MusicPlayerList
PropertyTypeLibrary/offset
PropertyTypeLibrary/PetrolMilage
PropertyTypeLibrary/RightLocked
PropertyTypeLibrary/RoundsPerMinetsFloat
PropertyTypeLibrary/RoundsPerMinute
PropertyTypeLibrary/smoothing
PropertyTypeLibrary/SPEED
PropertyTypeLibrary/Texture2
PropertyTypeLibrary/TextureCube
PropertyTypeLibrary/TurnSignals.LeftTurnedOn
PropertyTypeLibrary/TurnSignals.LeftTurnedOnOpacity
PropertyTypeLibrary/TurnSignals.RightTurnedOn
PropertyTypeLibrary/TurnSignals.RightTurnedOnOpacity
PropertyTypeLibrary/TAG_PROPERTY_TYPE_Transparent
Animation Clips/Animation Clip
Animation Clips/speedometer
Animation Clips/tachometer
Animation Clips/central part start animation clip
Animation Clips/turn signals start animation clip
Animation Clips/working right turn signal
Animation Clips/working left turn signal
Animation Clips/bargraph start animation clip
Screens/Screen/Root/Background/Background
Screens/Screen/Root/ContentMain/Center/Background/Image
Screens/Screen/Root/ContentMain/Center/Page Host Central Part/Music/MusicPlayer
Screens/Screen/Root/ContentMain/Center/Page Host Central Part/CarLockedUnlocked/CarLockedUnlocked
Screens/Screen/Root/ContentMain/Center/Page Host Central Part/<ResourceDictionaryInNode>
Screens/Screen/Root/ContentMain/Center/Page Host Central Part/Music
Screens/Screen/Root/ContentMain/Center/Page Host Central Part/CarLockedUnlocked
Screens/Screen/Root/ContentMain/Center/Background
Screens/Screen/Root/ContentMain/Center/Page Host Central Part
Screens/Screen/Root/ContentMain/General/TurnSignals
Screens/Screen/Root/ContentMain/General/BargraphBottom
Screens/Screen/Root/ContentMain/General/BargraphTop
Screens/Screen/Root/ContentMain/Right/Tachometer
Screens/Screen/Root/ContentMain/Left/Speedometer
Screens/Screen/Root/ContentMain/Center
Screens/Screen/Root/ContentMain/Button 2D Switch Central View
Screens/Screen/Root/ContentMain/General
Screens/Screen/Root/ContentMain/Right
Screens/Screen/Root/ContentMain/Left
Screens/Screen/Root/Background
Screens/Screen/Root/ContentMain
Screens/Screen/Root/Foreground
Screens/Screen/<ResourceDictionaryInNode>
Screens/Screen/Root
Screens/Screen
Material Types/VertexPhong
Material Types/VertexPhongCube
Material Types/VertexPhongMorph
Material Types/VertexPhongSkinned
Material Types/VertexPhongSkinnedMorph
Material Types/VertexPhongTextured
Material Types/VertexPhongTexturedCube
Material Types/VertexPhongTexturedMorph
Material Types/VertexPhongTexturedSkinned
Material Types/VertexPhongTexturedSkinnedMorph
Material Types/ColorTexture
Material Types/DefaultBlit
Material Types/FontDefault
Material Types/MaskTexture
Material Types/SweepTextures
Material Types/Textured
Materials/VertexPhongMaterial
Materials/VertexPhongCubeMaterial
Materials/VertexPhongMorphMaterial
Materials/VertexPhongSkinnedMaterial
Materials/VertexPhongSkinnedMorphMaterial
Materials/VertexPhongTexturedMaterial
Materials/VertexPhongTexturedCubeMaterial
Materials/VertexPhongTexturedMorphMaterial
Materials/VertexPhongTexturedSkinnedMaterial
Materials/VertexPhongTexturedSkinnedMorphMaterial
Materials/ColorTextureMaterial
Materials/DefaultBlitMaterial
Materials/FontDefaultMaterial
Materials/MaskTextureMaterial
Materials/SweepTexturesMaterial
Materials/TexturedMaterial
Brushes/DefaultBackground
Brushes/Texture Brush
Animation Data/Node2D.RenderTransformation - TRANSLATION_X
Animation Data/Node2D.RenderTransformation - TRANSLATION_X_1
Animation Data/Node.Opacity
Animation Data/Node2D.LayoutTransformation - SCALE_X
Animation Data/Node2D.LayoutTransformation - SCALE_Y
Animation Data/Node.Opacity_1
Animation Data/TurnSignals.RightTurnedOnOpacity
Animation Data/TurnSignals.LeftTurnedOnOpacity
Animation Data/Node.Opacity_2
Data Sources/GeneralSettings
Page Transitions/Default Transitions
Page Transitions/Transitions
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posX_128x128.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posX_64x64.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posX_32x32.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posX_16x16.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posX_8x8.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posX_4x4.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posX_2x2.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posX_1x1.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negX_128x128.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negX_64x64.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negX_32x32.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negX_16x16.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negX_8x8.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negX_4x4.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negX_2x2.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negX_1x1.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posY_128x128.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posY_64x64.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posY_32x32.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posY_16x16.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posY_8x8.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posY_4x4.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posY_2x2.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posY_1x1.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negY_128x128.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negY_64x64.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negY_32x32.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negY_16x16.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negY_8x8.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negY_4x4.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negY_2x2.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negY_1x1.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posZ_128x128.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posZ_64x64.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posZ_32x32.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posZ_16x16.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posZ_8x8.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posZ_4x4.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posZ_2x2.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posZ_1x1.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negZ_128x128.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negZ_64x64.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negZ_32x32.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negZ_16x16.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negZ_8x8.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negZ_4x4.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negZ_2x2.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negZ_1x1.png
Resource Files/Images/cubemaps/DefaultCubeMap.dds posX.png
Resource Files/Images/cubemaps/DefaultCubeMap.dds negX.png
Resource Files/Images/cubemaps/DefaultCubeMap.dds posY.png
Resource Files/Images/cubemaps/DefaultCubeMap.dds negY.png
Resource Files/Images/cubemaps/DefaultCubeMap.dds posZ.png
Resource Files/Images/cubemaps/DefaultCubeMap.dds negZ.png
Resource Files/Images/mipmaps/DefaultTextureImage_512x512.png
Resource Files/Images/mipmaps/DefaultTextureImage_256x256.png
Resource Files/Images/mipmaps/DefaultTextureImage_128x128.png
Resource Files/Images/mipmaps/DefaultTextureImage_64x64.png
Resource Files/Images/mipmaps/DefaultTextureImage_32x32.png
Resource Files/Images/mipmaps/DefaultTextureImage_16x16.png
Resource Files/Images/mipmaps/DefaultTextureImage_8x8.png
Resource Files/Images/mipmaps/DefaultTextureImage_4x4.png
Resource Files/Images/mipmaps/DefaultTextureImage_2x2.png
Resource Files/Images/mipmaps/DefaultTextureImage_1x1.png
Resource Files/Images/DefaultBackground.png
Resource Files/Images/DefaultTextureImage.png
Resource Files/Images/center background.png
Textures/DefaultBackground
Textures/Default Texture
Textures/Default Cube Map Texture
Textures/center background
Styles/Text Block 2D Style
/cluster
