precision mediump float; 

uniform vec4 ColorFull;
uniform vec4 ColorEmpty;
uniform float BlendIntensity;
uniform float Offset;
uniform float Smoothing;

varying vec2 vTexCoordRaw;

void main()
{
    float mask = smoothstep(vTexCoordRaw.x, vTexCoordRaw.x + Smoothing, Offset * (1.0 + Smoothing));
    
    vec4 mixedColor = mix(ColorEmpty, ColorFull, mask);

    gl_FragColor = mixedColor * BlendIntensity;
}