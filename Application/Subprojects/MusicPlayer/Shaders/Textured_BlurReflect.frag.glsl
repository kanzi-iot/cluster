precision mediump float;

uniform sampler2D Texture;
uniform sampler2D MaskTexture;
uniform float BlendIntensity;
uniform float VisibleProportion;
uniform float KeepIntensity;

varying vec2 vTexCoord;
varying vec2 v_texCoord;
varying vec2 v_blurTexCoords[8];
varying float vWorldFade;

void main()
{
    float blend = (1.0 - clamp(vTexCoord.y / VisibleProportion,0.0,1.0));
    
    vec4 color = vec4(0.0);
    color += texture2D(Texture, v_blurTexCoords[0])*0.0443683338718;
    color += texture2D(Texture, v_blurTexCoords[1])*0.0776744219933;
    color += texture2D(Texture, v_blurTexCoords[2])*0.115876621105;
    color += texture2D(Texture, v_blurTexCoords[3])*0.147308056121;
    color += texture2D(Texture, vTexCoord         )*0.159576912161;
    color += texture2D(Texture, v_blurTexCoords[4])*0.147308056121;
    color += texture2D(Texture, v_blurTexCoords[5])*0.115876621105;
    color += texture2D(Texture, v_blurTexCoords[6])*0.0776744219933;
    color += texture2D(Texture, v_blurTexCoords[7])*0.0443683338718;
    
    float a = blend * vWorldFade * BlendIntensity;
    
    gl_FragColor.rgb = color.rgb * a;
    gl_FragColor.a = color.a * a;
}
