// Use kanzi.hpp only when you are learning to develop Kanzi applications.
// To improve compilation time in production projects, include only the header files of the Kanzi functionality you are using.
#include <kanzi/kanzi.hpp>

// [CodeBehind libs inclusion]. Do not remove this identifier.

#if defined(TOPBARGRAPH_CODE_BEHIND_API) && !defined(ANDROID) && !defined(KANZI_API_IMPORT)
# include <topbargraph_code_behind_module.hpp>
#endif

#if defined(BARGRAPH_CODE_BEHIND_API) && !defined(ANDROID) && !defined(KANZI_API_IMPORT)
# include <bargraph_code_behind_module.hpp>
#endif

#if defined(BARGRAPH_CODE_BEHIND_API) && !defined(ANDROID) && !defined(KANZI_API_IMPORT)
# include <bargraph_code_behind_module.hpp>
#endif

#if defined(TURNSIGNALS_CODE_BEHIND_API) && !defined(ANDROID) && !defined(KANZI_API_IMPORT)
# include <turnsignals_code_behind_module.hpp>
#endif

#if defined(CARLOCKEDUNLOCKED_CODE_BEHIND_API) && !defined(ANDROID) && !defined(KANZI_API_IMPORT)
# include <carlockedunlocked_code_behind_module.hpp>
#endif

#if defined(MUSICPLAYER_CODE_BEHIND_API) && !defined(ANDROID) && !defined(KANZI_API_IMPORT)
# include <musicplayer_code_behind_module.hpp>
#endif

#if defined(BACKGROUND_CODE_BEHIND_API) && !defined(ANDROID) && !defined(KANZI_API_IMPORT)
# include <background_code_behind_module.hpp>
#endif

#if defined(TACHOMETER_CODE_BEHIND_API) && !defined(ANDROID) && !defined(KANZI_API_IMPORT)
# include <tachometer_code_behind_module.hpp>
#endif

#if defined(SPEEDOMETER_CODE_BEHIND_API) && !defined(ANDROID) && !defined(KANZI_API_IMPORT)
# include <speedometer_code_behind_module.hpp>
#endif

#if defined(CLUSTER_CODE_BEHIND_API) && !defined(ANDROID) && !defined(KANZI_CORE_API_IMPORT)
#include <Cluster_code_behind_module.hpp>
#endif

using namespace kanzi;

class Cluster : public ExampleApplication
{
public:

    void onConfigure(ApplicationProperties& configuration) override
    {
        configuration.binaryName = "cluster.kzb.cfg";
    }

    void onProjectLoaded() override
    {
        // Project file has been loaded from .kzb file.

        // Add initialization code here.
    }

    void registerMetadataOverride(ObjectFactory& factory) override
    {
        ExampleApplication::registerMetadataOverride(factory);

#if defined(CLUSTER_CODE_BEHIND_API) && !defined(ANDROID) && !defined(KANZI_CORE_API_IMPORT)
        ClusterCodeBehindModule::registerModule(getDomain());
#endif

        // [CodeBehind module inclusion]. Do not remove this identifier.

#if defined(TOPBARGRAPH_CODE_BEHIND_API) && !defined(ANDROID) && !defined(KANZI_API_IMPORT)
        TopBargraphCodeBehindModule::registerModule(getDomain());
#endif

#if defined(BARGRAPH_CODE_BEHIND_API) && !defined(ANDROID) && !defined(KANZI_API_IMPORT)
        BargraphCodeBehindModule::registerModule(getDomain());
#endif

#if defined(BARGRAPH_CODE_BEHIND_API) && !defined(ANDROID) && !defined(KANZI_API_IMPORT)
        BargraphCodeBehindModule::registerModule(getDomain());
#endif

#if defined(TURNSIGNALS_CODE_BEHIND_API) && !defined(ANDROID) && !defined(KANZI_API_IMPORT)
        TurnSignalsCodeBehindModule::registerModule(getDomain());
#endif

#if defined(CARLOCKEDUNLOCKED_CODE_BEHIND_API) && !defined(ANDROID) && !defined(KANZI_API_IMPORT)
        CarLockedUnlockedCodeBehindModule::registerModule(getDomain());
#endif

#if defined(MUSICPLAYER_CODE_BEHIND_API) && !defined(ANDROID) && !defined(KANZI_API_IMPORT)
        MusicPlayerCodeBehindModule::registerModule(getDomain());
#endif

#if defined(BACKGROUND_CODE_BEHIND_API) && !defined(ANDROID) && !defined(KANZI_API_IMPORT)
        BackgroundCodeBehindModule::registerModule(getDomain());
#endif

#if defined(TACHOMETER_CODE_BEHIND_API) && !defined(ANDROID) && !defined(KANZI_API_IMPORT)
        TachometerCodeBehindModule::registerModule(getDomain());
#endif

#if defined(SPEEDOMETER_CODE_BEHIND_API) && !defined(ANDROID) && !defined(KANZI_API_IMPORT)
        SpeedometerCodeBehindModule::registerModule(getDomain());
#endif
    }
};

Application* createApplication()
{
    return new Cluster;
}
